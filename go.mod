module github.com/sh0rez/tanka

go 1.12

require (
	github.com/Masterminds/semver v1.4.2
	github.com/alecthomas/chroma v0.6.6
	github.com/fatih/color v1.7.0
	github.com/google/go-jsonnet v0.13.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/pkg/errors v0.8.1
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/stretchr/objx v0.2.0
	github.com/thoas/go-funk v0.4.0
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/sys v0.0.0-20190616124812-15dcb6c0061f // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
